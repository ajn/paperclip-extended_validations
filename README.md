# Paperclip - Extended Validations

This library currently only consists of a single validation method for the [Paperclip](http://github.com/thoughtbot/paperclip) gem.

## validates_attachment_extension

Basically as simple as the other default Paperclip methods, you set the validation as so:

    class User < ActiveRecord::Base
  
      ##
      # Only accepts files with the extensions "jpg", "jpeg", "gif", "png"
      validates_attachment_extension :avatar,
        :extensions => %w[jpg jpeg gif png]
      
      ##
      # Set the :avatar attribute for Paperclip
      has_attached_file :avatar
      .....
  
    end
    
And that's it.

## Installation

Open up your Gemfile and add the following:

    gem "paperclip-extended_validations",
      :git      => "git://github.com/meskyanichi/paperclip-extended_validations.git",
      :require  => "extended_validations"
      
And run:

    bundle install

Now you should be able to validate on file extensions as well using Paperclip.