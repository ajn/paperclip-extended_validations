Gem::Specification.new do |gem|
  gem.name    = 'paperclip-extended_validations'
  gem.version = '0.1.2'
  gem.date    = Date.today.to_s

  gem.summary = "A very small library containing a validation method for paperclip that enables validations on file extension rather than content type."
  gem.description = "A very small library containing a validation method for paperclip that enables validations on file extension rather than content type."

  gem.authors  = ['Michael van Rooijen']
  gem.email    = 'meskyanichi@gmail.com'
  gem.homepage = 'http://github.com/meskyanichi/paperclip-extended_validations'

  gem.files = ['README.md'] + Dir['lib/**/*']
  gem.require_path = 'lib'
end